package net.advancedchemical;

public interface IPostInit extends IIniter {
	void onClientPostInit();
	void onServerPostInit();
}
