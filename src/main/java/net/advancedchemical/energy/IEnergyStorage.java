package net.advancedchemical.energy;

import net.minecraft.util.EnumFacing;

public interface IEnergyStorage extends IEnergyGenerator, IEnergyConsumer {
}
