package net.advancedchemical.energy;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;

public interface IEnergyGenerator extends IEnergyContainer {
	TileEntity getTileEntity();

	boolean isValidOutput(EnumFacing face);

	long getMaximumOut();

	@Override
	EnergyPointer getPointer();
}
