package net.advancedchemical.energy;

public class EnergyPointer {

	public final long MAXIMUM;
	private long current;

	public EnergyPointer(long maximum) {
		this.MAXIMUM = maximum;
	}

	public EnergyPointer(long maximum, long current) {
		this.MAXIMUM = maximum;
		if (current < 0)
			throw new RuntimeException("Can't set negativ Energy");
		if (current > maximum)
			throw new RuntimeException("Current (" + current + ") > Maximum (" + maximum + ")");
		this.current = current;
	}

	/***
	 * @return Returns Overflow
	 */
	public long addEnergy(long energy) {
		if (energy < 0)
			throw new RuntimeException("Can't add negativ Energy");
		if (MAXIMUM > this.current + energy) {
			long back = this.current + energy - MAXIMUM;
			this.current = MAXIMUM;
			return back;
		}
		this.current += energy;
		return 0;
	}
	
	public long addEnergyFrom(EnergyPointer other, long energy) {
		long left = other.addEnergy(this.subtractEnergy(energy));
		this.addEnergy(left);
		return left;
	}
	
	public long subtractEnergyFrom(EnergyPointer other, long energy) {
		return other.addEnergyFrom(this, energy);
	}

	/***
	 * @return Returns Subtracted Energy
	 */
	public long subtractEnergy(long energy) {
		if (energy < 0)
			throw new RuntimeException("Can't add subtract Energy");
		if (0 > this.current - energy) {
			long back = this.current;
			this.current = 0;
			return back;
		}
		this.current -= energy;
		return energy;
	}

	public void setEnergy(long energy) {
		if (energy < 0)
			throw new RuntimeException("Can't set negativ Energy");
		if (energy > MAXIMUM)
			throw new RuntimeException("Current (" + current + ") > Maximum (" + MAXIMUM + ")");
		this.current = energy;
	}

	public long getEnergy() {
		return current;
	}

}
