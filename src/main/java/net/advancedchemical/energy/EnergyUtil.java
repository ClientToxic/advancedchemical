package net.advancedchemical.energy;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.HashMap;

public final class EnergyUtil {

    private static HashMap<EntityPlayer, TileEntity> networkUpdates = new HashMap<>();

    private EnergyUtil() {

    }

    /***
     * Pushes Energy to IEnergyConsumer and IEnergyStorage
     *
     * @param generator
     */
    @SideOnly(Side.SERVER)
    public static <G extends TileEntity & IEnergyGenerator> void updateGenerator(G generator) {
        long toSpend = generator.getMaximumOut() < generator.getPointer().getEnergy() ? generator.getMaximumOut()
                : generator.getPointer().getEnergy();
        // Checking for surroundings
        int located = 0;
        ArrayList<TileEntity> locs = new ArrayList<>();
        for (EnumFacing face : EnumFacing.VALUES) {
            if (generator.isValidOutput(face)) {
                BlockPos pos = find(generator.getPos(), face);
                World world = generator.getWorld();
                if (world == null || !world.isRemote)
                    return;
                TileEntity tile = world.getTileEntity(pos);
                if (tile == null)
                    continue;
                if (tile instanceof IEnergyConsumer) {
                    IEnergyConsumer consumer = (IEnergyConsumer) tile;
                    if (!(consumer.isValidInput(invert(face))))
                        continue;
                    if (consumer.getPointer().MAXIMUM > consumer.getPointer().getEnergy()) {
                        located++;
                        locs.add(tile);
                    }
                } else if (tile instanceof IEnergyCable) {
                    IEnergyCable cable = (IEnergyCable) tile;
                    if (cable.getPointer().MAXIMUM > cable.getPointer().getEnergy()) {
                        located++;
                        locs.add(tile);
                    }
                }
            }
        }
        // Pushing Energy Around
        long lastSpend;
        do {
            lastSpend = toSpend;
            if (toSpend < located)
                break;
            long m = toSpend / located;
            ArrayList<TileEntity> remove = new ArrayList<>();
            for (TileEntity tile : locs) {
                IEnergyContainer container = (IEnergyContainer) tile;
                long left = container.getPointer().addEnergyFrom(generator.getPointer(), m);
                toSpend -= m - left;
                if (left > 0) {
                    located--;
                    remove.add(tile);
                }
            }
            for (TileEntity tile : remove)
                locs.remove(tile);
        } while (toSpend != lastSpend && located > 0);
    }

    @SideOnly(Side.SERVER)
    public static <C extends TileEntity & IEnergyCable> void updateCable(C cable) {
        long toSpend = cable.getMaximumOut() < cable.getPointer().getEnergy() ? cable.getMaximumOut()
                : cable.getPointer().getEnergy();
        // Checking for surroundings
        int located = 0;
        ArrayList<TileEntity> locs = new ArrayList<>();
        for (EnumFacing face : EnumFacing.VALUES) {
            BlockPos pos = find(cable.getPos(), face);
            World world = cable.getWorld();
            if (world == null || !world.isRemote)
                return;
            TileEntity tile = world.getTileEntity(pos);
            if (tile == null)
                continue;
            if (tile instanceof IEnergyConsumer) {
                IEnergyConsumer consumer = (IEnergyConsumer) tile;
                if (!(consumer.isValidInput(invert(face))))
                    continue;
                if (consumer.getPointer().MAXIMUM > consumer.getPointer().getEnergy()) {
                    located++;
                    locs.add(tile);
                }
            } else if (tile instanceof IEnergyCable) {
                IEnergyCable cable_ = (IEnergyCable) tile;
                if (cable_.getPointer().MAXIMUM > cable_.getPointer().getEnergy()
                        && cable_.getPointer().getEnergy() + 1 < cable.getPointer().getEnergy()) {
                    located++;
                    locs.add(tile);
                }
            }
        }
        // Pushing Energy Around
        long lastSpend;
        do {
            lastSpend = toSpend;
            if (toSpend < located)
                break;
            long m = toSpend / located;
            ArrayList<TileEntity> remove = new ArrayList<>();
            for (TileEntity tile : locs) {
                IEnergyContainer container = (IEnergyContainer) tile;
                long left = container.getPointer().addEnergyFrom(cable.getPointer(), m);
                toSpend -= m - left;
                if (left > 0) {
                    located--;
                    remove.add(tile);
                }
            }
            for (TileEntity tile : remove)
                locs.remove(tile);
        } while (toSpend != lastSpend && located > 0);
    }

    @SideOnly(Side.SERVER)
    public static <C extends TileEntity & IEnergyContainer> void updateConsumer(C consumer) {
        // Not Implemented yet
        // Don't know what to add
    }

    private static EnumFacing invert(EnumFacing face) {
        switch (face) {
            case DOWN:
                return EnumFacing.UP;
            case EAST:
                return EnumFacing.WEST;
            case NORTH:
                return EnumFacing.SOUTH;
            case SOUTH:
                return EnumFacing.NORTH;
            case UP:
                return EnumFacing.DOWN;
            case WEST:
                return EnumFacing.EAST;
            default:
                return EnumFacing.NORTH;
        }
    }

    private static BlockPos find(BlockPos pos, EnumFacing face) {
        switch (face) {
            case DOWN:
                return pos.down();
            case EAST:
                return pos.east();
            case NORTH:
                return pos.north();
            case SOUTH:
                return pos.south();
            case UP:
                return pos.up();
            case WEST:
                return pos.west();
            default:
                return pos;
        }
    }

    public static void disableBroadcast(EntityPlayer player) {
        if (networkUpdates.containsKey(player))
            networkUpdates.remove(player);
    }

    public static void enableBroadcast(EntityPlayer player, World world, BlockPos pos) {
        if (networkUpdates.containsKey(player))
            disableBroadcast(player);
        TileEntity tile = world.getTileEntity(pos);
        if (tile == null || !(tile instanceof IEnergyContainer))
            return;
        networkUpdates.put(player, tile);
    }
}