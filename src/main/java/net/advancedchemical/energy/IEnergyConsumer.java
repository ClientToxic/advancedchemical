package net.advancedchemical.energy;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;

public interface IEnergyConsumer extends IEnergyContainer {
	TileEntity getTileEntity();

	boolean isValidInput(EnumFacing face);

	@Override
	EnergyPointer getPointer();
}
