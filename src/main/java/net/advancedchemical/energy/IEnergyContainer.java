package net.advancedchemical.energy;

interface IEnergyContainer {
	EnergyPointer getPointer();
}
