package net.advancedchemical.energy;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;

public interface IEnergyCable extends IEnergyContainer {

	TileEntity getTileEntity();

	@Override
	EnergyPointer getPointer();
	
	long getMaximumOut();
}
