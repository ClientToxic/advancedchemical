package net.advancedchemical;

public interface IPreInit extends IIniter {
	void onClientPreInit();
	void onServerPreInit();
}
