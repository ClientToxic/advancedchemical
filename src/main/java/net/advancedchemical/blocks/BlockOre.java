package net.advancedchemical.blocks;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;

import java.util.Random;

public class BlockOre extends BlockBase {

    private final BlockOreHook hook;

    public BlockOre(String name, int height, int chance, int size, float hardness, float resistance, BlockOreHook hook) {
        super(name, Material.ROCK);
        this.setResistance(resistance);
        this.blockHardness = hardness;
        this.setSoundType(SoundType.STONE);
        this.hook = hook;
        OreGen.register(this, height, chance, size);
    }

    @Override
    public String getHarvestTool(IBlockState state) {
        return "pickaxe";
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return hook.getDropItem();
    }

    @Override
    public int damageDropped(IBlockState state) {
        return hook.getDropDamage();
    }

    @Override
    public int quantityDropped(IBlockState state, int fortune, Random random) {
        return hook.getAmount(fortune, random);
    }

    public interface BlockOreHook {
        /***
         *
         * @return Returns an Item or Block
         */
        <R extends net.minecraftforge.fml.common.registry.IForgeRegistryEntry.Impl<?>> R getDropItem();

        int getDropDamage();

        int getAmount(int fortune, Random random);
    }

}
