package net.advancedchemical.blocks;

import net.advancedchemical.IPreInit;
import net.advancedchemical.TheMod;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;

import java.util.ArrayList;

public class BlockBase extends Block implements IPreInit {
	
	protected static ArrayList<BlockBase> blocks = new ArrayList<>();
	private final String name;
	private Item item;
	private String oreName;
	
	public BlockBase(String name, Material materialIn) {
		super(materialIn);
		this.name = name;
		this.setRegistryName(TheMod.MODID, name);
		this.setUnlocalizedName(name);
		blocks.add(this);
		oreName = null;
		item = null;
	}
	
	public Item getItem() {
		return item;
	}
	
	@Override
	public void onClientPreInit() {
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(new ResourceLocation(TheMod.MODID, name), "inventory"));
	}
	
	@Override
	public void onServerPreInit() {
		GameRegistry.register(this);
		GameRegistry.register(item = (new ItemBlock(this).setRegistryName(TheMod.MODID, name).setUnlocalizedName(name)));
		if (oreName != null)
			OreDictionary.registerOre(oreName, this);
	}
	
	public BlockBase setDictionaryName(String type, String name) {
		oreName = type.toLowerCase() + name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
		return this;
	}
	
	public void setTab() {
		item.setCreativeTab(TheMod.MAIN_TAB);
		this.setCreativeTab(TheMod.MAIN_TAB);
	}
}
