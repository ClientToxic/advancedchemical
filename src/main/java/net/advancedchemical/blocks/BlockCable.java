package net.advancedchemical.blocks;

import net.advancedchemical.energy.IEnergyCable;
import net.advancedchemical.energy.IEnergyConsumer;
import net.advancedchemical.energy.IEnergyGenerator;
import net.advancedchemical.energy.IEnergyStorage;
import net.advancedchemical.tileentitys.TileEntityCable;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import java.util.List;
import java.util.Random;

public class BlockCable extends BlockMachine<TileEntityCable> implements ITileEntityProvider {
    public static final PropertyBool NORTH = PropertyBool.create("north");
    public static final PropertyBool EAST = PropertyBool.create("east");
    public static final PropertyBool SOUTH = PropertyBool.create("south");
    public static final PropertyBool WEST = PropertyBool.create("west");
    public static final PropertyBool UP = PropertyBool.create("up");
    public static final PropertyBool DOWN = PropertyBool.create("down");
    public static final PropertyBool NONE = PropertyBool.create("none");
    public static final PropertyBool SINGLE = PropertyBool.create("single");

    public static final AxisAlignedBB CENTER_AABB = new AxisAlignedBB(0.0625d * 6, 0.0625d * 6, 0.0625d * 6, 0.0625D * 10,
            0.0625D * 10, 0.0625D * 10);
    public static final AxisAlignedBB NORTH_AABB = new AxisAlignedBB(0.0625d * 6, 0.0625d * 6, 0, 0.0625D * 10, 0.0625D * 10,
            0.0625D * 10);
    public static final AxisAlignedBB SOUTH_AABB = new AxisAlignedBB(0.0625d * 6, 0.0625d * 6, 0.0625d * 6, 0.0625D * 10,
            0.0625D * 10, 0.0625D * 16);
    public static final AxisAlignedBB WEST_AABB = new AxisAlignedBB(0.0625d * 0, 0.0625d * 6, 0.0625d * 6, 0.0625D * 10,
            0.0625D * 10, 0.0625D * 10);
    public static final AxisAlignedBB EAST_AABB = new AxisAlignedBB(0.0625d * 6, 0.0625d * 6, 0.0625d * 6, 0.0625D * 16,
            0.0625D * 10, 0.0625D * 10);
    public static final AxisAlignedBB UP_AABB = new AxisAlignedBB(0.0625d * 6, 0.0625d * 6, 0.0625d * 6, 0.0625D * 10,
            0.0625D * 16, 0.0625D * 10);
    public static final AxisAlignedBB DOWN_AABB = new AxisAlignedBB(0.0625d * 6, 0.0625d * 0, 0.0625d * 6, 0.0625D * 10,
            0.0625D * 10, 0.0625D * 10);
    public final long MAX_OUT;

    public BlockCable(String name, long maxOut) {
        super(name, Material.CLOTH, TileEntityCable.class);
        this.useNeighborBrightness = true;
        this.setResistance(1f);
        this.setHardness(0.2f);
        this.MAX_OUT = maxOut;
    }

    @Override
    public TileEntity createTileEntity(World world, IBlockState state) {
        return new TileEntityCable(MAX_OUT);
    }

    @Override
    public void addCollisionBoxToList(IBlockState state, World world, BlockPos pos, AxisAlignedBB entityBox,
                                      List<AxisAlignedBB> collidingBoxes, Entity entityIn) {
        state = getActualState(state, world, pos);
        addCollisionBoxToList(pos, entityBox, collidingBoxes, CENTER_AABB);
        if (state.getValue(NORTH))
            addCollisionBoxToList(pos, entityBox, collidingBoxes, NORTH_AABB);
        if (state.getValue(SOUTH))
            addCollisionBoxToList(pos, entityBox, collidingBoxes, SOUTH_AABB);
        if (state.getValue(EAST))
            addCollisionBoxToList(pos, entityBox, collidingBoxes, EAST_AABB);
        if (state.getValue(WEST))
            addCollisionBoxToList(pos, entityBox, collidingBoxes, WEST_AABB);
        if (state.getValue(UP))
            addCollisionBoxToList(pos, entityBox, collidingBoxes, UP_AABB);
        if (state.getValue(DOWN))
            addCollisionBoxToList(pos, entityBox, collidingBoxes, DOWN_AABB);
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        state = getActualState(state, source, pos);
        AxisAlignedBB aabb = CENTER_AABB;
        if (state.getValue(NORTH))
            aabb = aabb.union(NORTH_AABB);
        if (state.getValue(SOUTH))
            aabb = aabb.union(SOUTH_AABB);
        if (state.getValue(EAST))
            aabb = aabb.union(EAST_AABB);
        if (state.getValue(WEST))
            aabb = aabb.union(WEST_AABB);
        if (state.getValue(UP))
            aabb = aabb.union(UP_AABB);
        if (state.getValue(DOWN))
            aabb = aabb.union(DOWN_AABB);
        return aabb;
    }

    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess world, BlockPos pos) {
        boolean[] can = new boolean[6];
        state = state.withProperty(NORTH, can[0] = canConnectTo(world, pos.north())).withProperty(EAST, can[1] = canConnectTo(world, pos.east()))
                .withProperty(SOUTH, can[2] = canConnectTo(world, pos.south())).withProperty(WEST, can[3] = canConnectTo(world, pos.west()))
                .withProperty(UP, can[4] = canConnectTo(world, pos.up())).withProperty(DOWN, can[5] = canConnectTo(world, pos.down()));
        byte c = 0;
        for (boolean bool : can) if (bool) c++;
        state = state.withProperty(SINGLE, c == 1).withProperty(NONE, c == 0);
        return state;
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, new IProperty[]{NORTH, SOUTH, EAST, WEST, UP, DOWN, NONE, SINGLE});
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.blockState.getBaseState();
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return 0;
    }

    @Override
    public BlockRenderLayer getBlockLayer() {
        return BlockRenderLayer.SOLID;
    }

    @Override
    public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side) {
        return true;
    }

    public boolean canConnectTo(IBlockAccess worldIn, BlockPos pos) {
        IBlockState iblockstate = worldIn.getBlockState(pos);
        Block block = iblockstate.getBlock();
        return block instanceof BlockCable || block instanceof BlockMachine<?> || block instanceof IEnergyCable
                || block instanceof IEnergyConsumer || block instanceof IEnergyGenerator || block instanceof IEnergyStorage;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityCable(MAX_OUT);
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return Item.getItemFromBlock(this);
    }
}