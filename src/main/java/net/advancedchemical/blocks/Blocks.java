package net.advancedchemical.blocks;

import java.util.Random;

import net.advancedchemical.blocks.BlockOre.BlockOreHook;
import net.advancedchemical.items.Items;
import net.minecraft.item.Item;

public final class Blocks {

	public static final BlockBase COPPER_ORE = new BlockOre("copper_ore", 64, 3, 3, 5f, 10f, new BlockOreHook() {
		@Override
		public Item getDropItem() {
			return COPPER_ORE.getItem();
		}

		public int getDropDamage() {
			return 0;
		}

		@Override
		public int getAmount(int fortune, Random random) {
			return 1;
		}
	}).setDictionaryName("ore", "copper");

	static {

	}

	public static BlockBase[] getBlocks() {
		BlockBase[] array = new BlockBase[BlockBase.blocks.size()];
		BlockBase.blocks.toArray(array);
		return array;
	}
}
