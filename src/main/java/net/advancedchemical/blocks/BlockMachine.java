package net.advancedchemical.blocks;

import net.advancedchemical.TheMod;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockMachine<TE extends TileEntity> extends BlockBase implements ITileEntityProvider {

    private Class<? extends TE> tileEntity;

    public BlockMachine(String name, Material materialIn, Class<? extends TE> tileEntity) {
        super(name, materialIn);
        this.tileEntity = tileEntity;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        try {
            return tileEntity.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            TheMod.LOGGER.error(e);
            TheMod.LOGGER.error("Couldn't create instance of TE");
            return null;
        }
    }
}