package net.advancedchemical.blocks;

import net.advancedchemical.IInit;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkGenerator;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class OreGen implements IInit, IWorldGenerator {

    private static ArrayList<WorldGenerator> gens = new ArrayList<>();
    private static HashMap<WorldGenerator, int[]> reference = new HashMap<>();

    public static void register(BlockOre blockOre, int height, int chance, int size) {
        WorldGenerator gen = new WorldGenMinable(
                blockOre.getDefaultState(), size);
        gens.add(gen);
        reference.put(gen, new int[]{chance, height});
    }

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator,
                         IChunkProvider chunkProvider) {
        for (WorldGenerator gen : gens) {
            int[] ref = reference.get(gen);
            runGenerator(gen, world, random, chunkX, chunkZ, ref[0], 0, ref[1]);
        }
    }

    private void runGenerator(WorldGenerator generator, World world, Random rand, int chunk_X, int chunk_Z,
                              int chancesToSpawn, int minHeight, int maxHeight) {
        if (minHeight < 0 || maxHeight > 256 || minHeight > maxHeight)
            throw new IllegalArgumentException("Illegal Height Arguments for WorldGenerator");

        int heightDiff = maxHeight - minHeight + 1;
        for (int i = 0; i < chancesToSpawn; i++) {
            int x = chunk_X * 16 + rand.nextInt(16);
            int y = minHeight + rand.nextInt(heightDiff);
            int z = chunk_Z * 16 + rand.nextInt(16);
            generator.generate(world, rand, new BlockPos(x, y, z));
        }
    }

    @Override
    public void onClientInit() {
    }

    @Override
    public void onServerInit() {
    }

}
