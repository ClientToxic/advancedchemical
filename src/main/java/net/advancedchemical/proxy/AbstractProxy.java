package net.advancedchemical.proxy;

import net.advancedchemical.IInit;
import net.advancedchemical.IPostInit;
import net.advancedchemical.IPreInit;
import net.minecraftforge.fml.relauncher.Side;

public abstract class AbstractProxy {

	public abstract void preInit(IPreInit[] initer);

	public abstract void init(IInit[] initer);

	public abstract void postInit(IPostInit[] initer);
}
