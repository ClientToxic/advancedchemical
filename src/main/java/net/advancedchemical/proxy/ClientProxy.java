package net.advancedchemical.proxy;

import net.advancedchemical.IInit;
import net.advancedchemical.IPostInit;
import net.advancedchemical.IPreInit;

public class ClientProxy extends AbstractProxy {
	
	@Override
	public void preInit(IPreInit[] initer) {
		for (IPreInit init : initer)
			init.onServerPreInit();
		for (IPreInit init : initer)
			init.onClientPreInit();
	}
	
	@Override
	public void init(IInit[] initer) {
		for (IInit init : initer)
			init.onServerInit();
		for (IInit init : initer)
			init.onClientInit();
	}
	
	@Override
	public void postInit(IPostInit[] initer) {
		for (IPostInit init : initer)
			init.onServerPostInit();
		for (IPostInit init : initer)
			init.onClientPostInit();
	}
}
