package net.advancedchemical.proxy;

import net.advancedchemical.IInit;
import net.advancedchemical.IPostInit;
import net.advancedchemical.IPreInit;

public class ServerProxy extends AbstractProxy {
	
	@Override
	public void preInit(IPreInit[] initer) {
		for (IPreInit init : initer)
			init.onServerPreInit();
	}
	
	@Override
	public void init(IInit[] initer) {
		for (IInit init : initer)
			init.onServerInit();
	}
	
	@Override
	public void postInit(IPostInit[] initer) {
		for (IPostInit init : initer)
			init.onServerPostInit();
	}
}
