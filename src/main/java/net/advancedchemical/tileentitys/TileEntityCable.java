package net.advancedchemical.tileentitys;

import net.advancedchemical.blocks.BlockCable;
import net.advancedchemical.energy.EnergyPointer;
import net.advancedchemical.energy.IEnergyCable;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileEntityCable extends TileEntity implements IEnergyCable {

    private EnergyPointer pointer;

    public TileEntityCable(long maximum) {
        pointer = new EnergyPointer(maximum);
    }

    @Override
    public TileEntity getTileEntity() {
        return this;
    }

    @Override
    public EnergyPointer getPointer() {
        return pointer;
    }

    @Override
    public long getMaximumOut() {
        return ((BlockCable) this.getBlockType()).MAX_OUT;
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setLong("energy", pointer.getEnergy());
        return super.writeToNBT(compound);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if (compound.hasKey("energy"))
            pointer.setEnergy(compound.getLong("energy"));
    }
}
