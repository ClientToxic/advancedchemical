package net.advancedchemical;

import net.advancedchemical.blocks.BlockBase;
import net.advancedchemical.blocks.Blocks;
import net.advancedchemical.creativetab.MainCreativeTab;
import net.advancedchemical.items.ItemBase;
import net.advancedchemical.items.Items;
import net.advancedchemical.network.NetworkRegister;
import net.advancedchemical.proxy.AbstractProxy;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

@Mod(modid = TheMod.MODID, version = TheMod.VERSION)
public class TheMod {
	
	// Infos
	public static final String MODID = "ac1";
	public static final String VERSION = "1.0";
	// CreativeTabs
	public final static MainCreativeTab MAIN_TAB = null;
	// Logger
	public static Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger("AC1");
	// Proxy
	@SidedProxy(serverSide = "net.advancedchemical.proxy.ServerProxy", clientSide = "net.advancedchemical.proxy.ClientProxy", modId = MODID)
	public static AbstractProxy PROXY;
	// Networking Stuff
	public static SimpleNetworkWrapper NETWORKING;
	// Initer
	static ArrayList<IPreInit> preIniter = new ArrayList<>();
	static ArrayList<IInit> initer = new ArrayList<>();
	static ArrayList<IPostInit> postIniter = new ArrayList<>();
	
	static {
		registerInitHook(new NetworkRegister());
		for (BlockBase base : Blocks.getBlocks())
			registerInitHook(base);
		for (ItemBase base : Items.getItems())
			registerInitHook(base);
	}
	
	public static <I extends IIniter> void registerInitHook(I initHook) {
		if (initHook instanceof IPreInit)
			preIniter.add((IPreInit) initHook);
		if (initHook instanceof IInit)
			initer.add((IInit) initHook);
		if (initHook instanceof IPostInit)
			postIniter.add((IPostInit) initHook);
	}
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		LOGGER.info("Pre-Init");
		
		IPreInit[] initer = new IPreInit[preIniter.size()];
		preIniter.toArray(initer);
		PROXY.preInit(initer);
		
		if (MAIN_TAB == null) {
			try {
				Field field = null;
				field = this.getClass().getDeclaredField("MAIN_TAB");
				field.setAccessible(true);
				
				Field modifiersField = Field.class.getDeclaredField("modifiers");
				modifiersField.setAccessible(true);
				modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
				
				field.set(null, new MainCreativeTab());
			} catch (NoSuchFieldException | IllegalAccessException e) {
				LOGGER.fatal("Couldn't set CreativeTab");
				throw new RuntimeException(e);
			}
		}
		
		for (BlockBase base : Blocks.getBlocks())
			base.setTab();
		for (ItemBase base : Items.getItems())
			base.setTab();
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event) {
		LOGGER.info("Init");
		
		NETWORKING = NetworkRegistry.INSTANCE.newSimpleChannel(MODID);
		
		IInit[] initer = new IInit[this.initer.size()];
		this.initer.toArray(initer);
		PROXY.init(initer);
	}
	
	@EventHandler
	public void postInit(FMLPreInitializationEvent event) {
		LOGGER.info("Post-Init");
		
		IPostInit[] initer = new IPostInit[postIniter.size()];
		postIniter.toArray(initer);
		PROXY.postInit(initer);
		
		LOGGER.info("Initialization of AC1 successful");
	}
}
