package net.advancedchemical.gui;

public interface IEnergyGui {

	void setEnergy(long maximum, long energy);

}
