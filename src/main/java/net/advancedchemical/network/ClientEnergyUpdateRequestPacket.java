package net.advancedchemical.network;

import io.netty.buffer.ByteBuf;
import net.advancedchemical.energy.EnergyUtil;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

public class ClientEnergyUpdateRequestPacket implements IMessage {

    public BlockPos pos;
    public World world;

    public ClientEnergyUpdateRequestPacket() {
        super();
        pos = null;
        world = null;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        if (buf.readByte() == 0)
            return;
        world = DimensionManager.getWorld(buf.readInt());
        pos = new BlockPos(buf.readInt(), buf.readInt(), buf.readInt());
    }

    @Override
    public void toBytes(ByteBuf buf) {
        if (pos == null)
            buf.writeByte(1);
        else {
            buf.writeByte(0);
            buf.writeInt(world.provider.getDimension());
            buf.writeInt(pos.getX());
            buf.writeInt(pos.getY());
            buf.writeInt(pos.getZ());
        }
    }

    public static class Handler implements IMessageHandler<ClientEnergyUpdateRequestPacket, IMessage> {
        @Override
        public IMessage onMessage(ClientEnergyUpdateRequestPacket message, MessageContext ctx) {
            if (ctx.side == Side.SERVER) {
                if (message.pos == null)
                    EnergyUtil.disableBroadcast(ctx.getServerHandler().playerEntity);
                else
                    EnergyUtil.enableBroadcast(ctx.getServerHandler().playerEntity, message.world, message.pos);
            }
            return null;
        }
    }
}
