package net.advancedchemical.network;

import net.advancedchemical.IInit;
import net.advancedchemical.TheMod;
import net.minecraftforge.fml.relauncher.Side;

public class NetworkRegister implements IInit {
    @Override
    public void onClientInit() {
    }

    @Override
    public void onServerInit() {
        TheMod.NETWORKING.registerMessage(ClientEnergyUpdateRequestPacket.Handler.class, ClientEnergyUpdateRequestPacket.class, 0, Side.SERVER);
        TheMod.NETWORKING.registerMessage(ClientEnergyUpdatePacket.Handler.class, ClientEnergyUpdatePacket.class, 1, Side.CLIENT);
    }
}
