package net.advancedchemical.network;

import io.netty.buffer.ByteBuf;
import net.advancedchemical.gui.IEnergyGui;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

public class ClientEnergyUpdatePacket implements IMessage {

    public long maximum;
    public long energy;

    public ClientEnergyUpdatePacket() {
        super();
    }

    public ClientEnergyUpdatePacket(long maximum, long energy) {
        this.maximum = maximum;
        this.energy = energy;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        maximum = buf.readLong();
        energy = buf.readLong();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeLong(maximum);
        buf.writeLong(energy);
    }

    public static class Handler implements IMessageHandler<ClientEnergyUpdatePacket, IMessage> {


        @Override
        public IMessage onMessage(ClientEnergyUpdatePacket message, MessageContext ctx) {
            if (ctx.side == Side.CLIENT) {
                if (Minecraft.getMinecraft().currentScreen instanceof IEnergyGui)
                    ((IEnergyGui) Minecraft.getMinecraft().currentScreen).setEnergy(message.maximum, message.energy);
            }
            return null;
        }
    }
}
