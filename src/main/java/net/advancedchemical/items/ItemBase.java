package net.advancedchemical.items;

import net.advancedchemical.IPreInit;
import net.advancedchemical.TheMod;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;

import java.util.ArrayList;

public class ItemBase extends Item implements IPreInit {
    protected static ArrayList<ItemBase> items = new ArrayList<>();
    private final String name;
    private String oreName;

    public ItemBase(String name) {
        this.name = name;
        this.setRegistryName(TheMod.MODID, name);
        this.setUnlocalizedName(name);
        items.add(this);
        oreName = null;
    }

    @Override
    public void onClientPreInit() {
        ModelLoader.setCustomModelResourceLocation(this, 0,
                new ModelResourceLocation(new ResourceLocation(TheMod.MODID, name), "inventory"));
    }

    @Override
    public void onServerPreInit() {
        GameRegistry.register(this);
        if (oreName != null)
            OreDictionary.registerOre(oreName, this);
    }

    public ItemBase setDictionaryName(String type, String name) {
        oreName = type.toLowerCase() + name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        return this;
    }

    public String getName() {
        return name;
    }

    public void setTab() {
        this.setCreativeTab(TheMod.MAIN_TAB);
    }
}
