package net.advancedchemical.items;

public final class Items {
	
	public static final ItemBase COPPER_INGOT = new ItemBase("copper_ingot");
	public static final ItemBase COPPER_DUST = new ItemBase("copper_dust");
	
	public static final ItemBase IRON_DUST = new ItemBase("iron_dust");
	
	static {
	
	}
	
	public static ItemBase[] getItems() {
		ItemBase[] items = new ItemBase[ItemBase.items.size()];
		ItemBase.items.toArray(items);
		return items;
	}
}
