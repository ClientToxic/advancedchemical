package net.advancedchemical.intelli_pipe;

public class Connection<F extends IIntelligentPipe, S extends IIntelligentPipe> {
    public final F first;
    public final S second;

    public Connection(F first, S second) {
        this.first = first;
        this.second = second;
    }
}

