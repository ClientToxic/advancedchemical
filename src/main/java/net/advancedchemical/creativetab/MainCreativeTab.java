package net.advancedchemical.creativetab;

import net.advancedchemical.TheMod;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

public class MainCreativeTab extends CreativeTabs {

	public MainCreativeTab() {
		super("ac1_tab");
		this.setBackgroundImageName("item_search.png");
		TheMod.LOGGER.debug("CreativeTab created");
	}

	@Override
	public Item getTabIconItem() {
		return Items.GLOWSTONE_DUST;
	}
	
	@Override
	public boolean hasSearchBar() {
		return true;
	}
	
}
